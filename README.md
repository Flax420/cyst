# CYST
A CLI Youtube video player written in C inspired by mps-youtube. 

# About
The idea started as I was trying out mps-youtube and it not supporting features that I wanted. I could just have forked it.
But I felt that it would be unnecessary and a good opportunity to dip my toes into the C programming language.
As this is my first time using C it will probably not be very good and in the end I might switch to Rust, a language I am more familiar with.

# Dependencies
mpv
ncurses
libcurl
